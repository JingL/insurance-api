# insurance-api-ms

## Description
The repository of user insurance policies microservice

## Scripts

```bash
npm install # install all the dependencies
npm start # run the API by default in localhost:1303
npm test # run testing

npm run relase:<type> # release version with following types: patch, major, minor
```
