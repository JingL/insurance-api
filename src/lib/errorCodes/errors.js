const InvalidSecret = {
  code: 401,
  message: 'Invalid Secret'
};

const InvalidContentType = {
  code: 401,
  message: 'Invalid ContentType'
};

const UserNotFound = {
  code: 404,
  message: 'User not found'
};

const InvalidUser = {
  code: 404,
  message: 'User role is NOT admin'
};

const UserNotFoundOrInvalidUser = {
  code: 404,
  message: 'User not found or user role is NOT admin'
};

const PolicyNotFound = {
  code: 404,
  message: 'Policy not found'
};

const MissingFieldId = {
  code: 400,
  message: 'Missing parameter Id'
};

const MissingFieldName = {
  code: 400,
  message: 'Missing parameter Name'
};

module.exports = {
  InvalidSecret,
  InvalidContentType,
  UserNotFound,
  InvalidUser,
  UserNotFoundOrInvalidUser,
  PolicyNotFound,
  MissingFieldId,
  MissingFieldName
}
