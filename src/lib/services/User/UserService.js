const request = require('superagent');

const logger = require('logger').createLogger();

const { USER_DATA_API, TIME_OUT } = require('../../config/config');

class UserService {
  getUsers() {
    return new Promise(function (resolve, reject) {
      request
        .get(USER_DATA_API)
        .set('Content-Type', 'application/json')
        .timeout(TIME_OUT)
        .end(function (err, res) {
          if (err) {
            logger.error(`Get users data API with error - ${JSON.stringify(err)}`);
            reject(err);
          } else {
            resolve(JSON.parse(res.text).clients);
          }
        });
    });
  }
}

module.exports = new UserService();
