const request = require('superagent');

const logger = require('logger').createLogger();

const { POLICY_DATA_API, TIME_OUT } = require('../../config/config');

class PolicyService {
  getPolicies() {
    return new Promise(function (resolve, reject) {
      request
        .get(POLICY_DATA_API)
        .set('Content-Type', 'application/json')
        .timeout(TIME_OUT)
        .end(function (err, res) {
          if (err) {
            logger.error(`Get policies data API with error - ${JSON.stringify(err)}`);
            reject(err);
          } else {
            resolve(JSON.parse(res.text).policies);
          }
        });
    });
  }
}

module.exports = new PolicyService();
