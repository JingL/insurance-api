const logger = require('logger').createLogger();

const UserService = require('../services/User').UserService;
const Check = require('../utils/Check');

class UserHandler {

  async getUserDataById({ body }, res) {
    try {
      const id = body ? body.id : '';

      if (!id) {
        return Check.missingFieldId(res);
      }

      const users = await UserService.getUsers();
      const user = users.find(user => user.id === id);

      if (!user) {
        return Check.userNotFound(res);
      }

      return res.status(200).send({
        result: { user }
      });

    } catch(e) {
      logger.error(`Error while getting user data by Id: ${JSON.stringify(e)}`);
      return res.status(420).send({
        error: e.message,
        success: false
      });
    }
  }

  async getUserDataByName({ body }, res) {
    try {
      const name = body ? body.name : '';

      if (!name) {
        return Check.missingFieldName(res);
      }

      const users = await UserService.getUsers();
      const user = users.filter(elem => elem.name === name);

      if (user.length < 1) {
        return Check.userNotFound(res);
      }

      return res.status(200).send({
        result: { user }
      });
    } catch (e) {
      logger.error(`Error while getting user data by name: ${JSON.stringify(e)}`);
      return res.status(420).send({
        error: e.message,
        success: false
      });
    }
  }
}

module.exports = new UserHandler();
