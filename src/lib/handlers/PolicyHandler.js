const logger = require('logger').createLogger();

const UserService = require('../services/User').UserService;
const PolicyService = require('../services/Policy').PolicyService;
const Check = require('../utils/Check');

class PolicyHandler {
  async getPoliciesByUserName({ body }, res) {
    try {
      const name = body ? body.name : '';

      if (!name) {
        return Check.missingFieldName(res);
      }
      const users = await UserService.getUsers();
      const user = users.filter(elem => elem.name === name && elem.role === 'admin');

      if (user.length < 1) {
        return Check.userNotFoundOrInvalidUser(res);
      }

      const policies = await PolicyService.getPolicies();
      const userPolicies = user.map(elem => policies.filter(policy => policy.clientId === elem.id));

      if (userPolicies.length < 1) {
        return Check.policyNotFound(res);
      }

      return res.status(200).send({
        result: { userPolicies }
      });

    } catch(e) {
      logger.error(`Error while getting policy by user name: ${JSON.stringify(e)}`);
      return res.status(420).send({
        error: e.message,
        success: false
      });
    }
  }

  async getUserByPolicyId({ body }, res) {
    try {
      const policyId = body ? body.id : '';

      if (!policyId) {
        return Check.missingFieldId(res);
      }

      const policies = await PolicyService.getPolicies();
      const policy = policies.find(elem => elem.id === policyId);

      if (!policy) {
        return Check.policyNotFound(res);
      }

      const users = await UserService.getUsers();
      const user = users.find(elem => elem.id === policy.clientId);

      if (!user) {
        return Check.userNotFound(res);
      }

      if (user && user.role !== 'admin') {
        return Check.invalidUser(res);
      }

      return res.status(200).send({
        result: { user }
      });
    } catch (e) {
      logger.error(`Error while getting user by policy Id ${JSON.stringify(e)}`);
      return res.status(420).send({
        error: e.message,
        success: false
      });
    }
  }
}

module.exports = new PolicyHandler();
