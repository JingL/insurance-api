const express = require('express');

const security = require('../middlewares/security');
const PolicyHandler = require('../handlers').PolicyHandler;

const router = new express.Router();

router
  .route('/byUserName')
  .all(security)
  .post(PolicyHandler.getPoliciesByUserName);

router
  .route('/byId')
  .all(security)
  .post(PolicyHandler.getUserByPolicyId);

module.exports = router;
