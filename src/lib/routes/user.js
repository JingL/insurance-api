'use strict';

const express = require('express');

const security = require('../middlewares/security');
const UserHandler = require('../handlers').UserHandler;

const router = express.Router();

router
  .route('/byId')
  .all(security)
  .post(UserHandler.getUserDataById);

router
  .route('/byName')
  .all(security)
  .post(UserHandler.getUserDataByName);

module.exports = router;
