const {
  PORT = '1303',
  SECRET = 'insuranceSuperSecureSecret',
  USER_DATA_API = 'http://www.mocky.io/v2/5808862710000087232b75ac',
  POLICY_DATA_API = 'http://www.mocky.io/v2/580891a4100000e8242b75c5',
  TIME_OUT = '15000',
  BODY_REQUEST_LIMIT = '50mb'
} = process.env;

module.exports = {
  PORT,
  SECRET,
  USER_DATA_API,
  POLICY_DATA_API,
  TIME_OUT,
  BODY_REQUEST_LIMIT
};
