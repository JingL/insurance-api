'use strict';

const { InvalidSecret, InvalidContentType, MissingFieldId, MissingFieldName, UserNotFound, InvalidUser,
  UserNotFoundOrInvalidUser, PolicyNotFound } = require('../errorCodes/errors');

class Check {
  invalidSecret(res) {
    return res.status(InvalidSecret.code).send({
      error: InvalidSecret.message,
      success: false
    });
  }

  invalidContentType(res) {
    return res.status(InvalidContentType.code).send({
      error: InvalidContentType.message,
      success: false
    });
  }
  missingFieldId(res) {
    return res.status(MissingFieldId.code).send({
      error: MissingFieldId.message,
      success: false
    });
  }

  missingFieldName(res) {
    return res.status(MissingFieldName.code).send({
      error: MissingFieldName.message,
      success: false
    });
  }

  userNotFound(res) {
    return res.status(UserNotFound.code).send({
      error: UserNotFound.message,
      success: false
    });
  }

  invalidUser(res) {
    return res.status(InvalidUser.code).send({
      error: InvalidUser.message,
      success: false
    });
  };

  userNotFoundOrInvalidUser(res) {
    return res.status(UserNotFoundOrInvalidUser.code).send({
      error: UserNotFoundOrInvalidUser.message,
      success: false
    });
  }

  policyNotFound(res) {
    return res.status(PolicyNotFound.code).send({
      error: PolicyNotFound.message,
      success: false
    });
  }
}

module.exports = new Check();
