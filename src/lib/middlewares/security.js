const Check = require('../utils/Check');

const { SECRET } = require('../config/config');

module.exports = (req, res, next) => {
  if (req.header('x-insurance-identity') !== SECRET) {
    return Check.invalidSecret(res);
  }
  if (req.header('Content-Type') !== 'application/json'){
    return Check.invalidContentType(res);
  }
  next();
}
