const express = require('express');
const bodyParser = require('body-parser');
const logger = require('logger').createLogger();

const { PORT, BODY_REQUEST_LIMIT } = require('./config/config');
const UserRoutes = require('./routes').UserRoutes;
const PolicyRoutes = require('./routes').PolicyRoutes;
const cors = require('./middlewares/cors');

const app = express();

app.use(bodyParser.json({
  limit: BODY_REQUEST_LIMIT
}));

app.use(cors);

app.get('/', function (req, res) {
  res.send('API is running');
});

app.get('/ping', function (req, res) {
  res.send('pong');
});

app.use('/insurance/user', UserRoutes);
app.use('/insurance/policy', PolicyRoutes);

const start = function () {
  try {
    app.listen(PORT, function () {
      logger.info(`Running on the port ${PORT}`);
    });
  } catch (e) {
    logger.error(e);
    process.exit(1);
  }
}

module.exports = {
  app,
  start
};
