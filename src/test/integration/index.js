const request = require('supertest');
const test = require('tape');

const { app, start } = require('../../lib/server');

test('/', function (assert) {
  request(app)
    .get('/')
    .expect(200)
    .end(function (err, res) {
      assert.error(err, 'should not throw error');
      assert.equals(res.text, 'API is running', 'should respond with API is running');
      assert.end();
    });
});

test('/ping', function (assert) {
  request(app)
    .get('/ping')
    .expect(200)
    .end(function (err, res) {
      assert.error(err, 'should not throw error');
      assert.equals(res.text, 'pong', 'should respond with pong');
      assert.end();
    });
});
