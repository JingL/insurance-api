const request = require('supertest');
const test = require('tape');

const { app, start } = require('../../lib/server');

const { SECRET } = require('../../lib/config/config');
const { InvalidSecret, InvalidContentType, MissingFieldId, MissingFieldName, UserNotFound } = require('../../lib/errorCodes/errors');

test('/insurance/user/byId', function (t) {

  t.test('with wrong secret', function (assert) {
    const expected = { error: InvalidSecret.message, success: false };

    request(app)
      .post('/insurance/user/byId')
      .expect(InvalidSecret.code)
      .set('x-insurance-identity', 'test')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidSecret');
        assert.end();
      });
  });

  t.test('with wrong content-type', function (assert) {
    const expected = { error: InvalidContentType.message, success: false };

    request(app)
      .post('/insurance/user/byId')
      .expect(InvalidContentType.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidContentType');
        assert.end();
      });
  });

  t.test('without body', function (assert) {
    const expected = { error: MissingFieldId.message, success: false };

    request(app)
      .post('/insurance/user/byId')
      .expect(MissingFieldId.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with MissingFieldId');
        assert.end();
      });
  });

  t.test('with wrong userId', function (assert) {
    const expected = { error: UserNotFound.message, success: false };

    request(app)
      .post('/insurance/user/byId')
      .expect(UserNotFound.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .send({
        id: 'wrongId'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with UserNotFound');
        assert.end();
      });
  });

  t.test('with correct param', function (assert) {
    const expected = { result: { user: { id: 'cbce10d7-b281-4bbd-a94e-447c63ea914e', name: 'Buck', email: 'buckblankenship@quotezart.com', role: 'user' }}};

    request(app)
      .post('/insurance/user/byId')
      .expect(200)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .send({
        id: 'cbce10d7-b281-4bbd-a94e-447c63ea914e'
      })
      .end(function (err, res) {
        assert.error(err, 'should not throw error');
        assert.deepEqual(res.body, expected, 'should respond with the information of user');
        assert.end();
      });
  });

});

test('/insurance/user/byName', function (t) {

  t.test('with wrong secret', function (assert) {
    const expected = { error: InvalidSecret.message, success: false };

    request(app)
      .post('/insurance/user/byName')
      .expect(InvalidSecret.code)
      .set('x-insurance-identity', 'test')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidSecret');
        assert.end();
      });
  });

  t.test('with wrong content-type', function (assert) {
    const expected = { error: InvalidContentType.message, success: false };

    request(app)
      .post('/insurance/user/byName')
      .expect(InvalidContentType.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidContentType');
        assert.end();
      });
  });

  t.test('without body', function (assert) {
    const expected = { error: MissingFieldName.message, success: false };

    request(app)
      .post('/insurance/user/byName')
      .expect(MissingFieldName.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with MissingFieldName');
        assert.end();
      });
  });

  t.test('with wrong userName', function (assert) {
    const expected = { error: UserNotFound.message, success: false };

    request(app)
      .post('/insurance/user/byName')
      .expect(UserNotFound.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .send({
        name: 'wrongName'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with UserNotFound');
        assert.end();
      });
  });

  t.test('with correct param', function (assert) {
    const expected = { result: { user: [{ id: '364b59fa-ecfb-41cb-abbf-7b3f4b05bd93', name: 'Corina', email: 'corinablankenship@quotezart.com', role: 'admin' }]}};

    request(app)
      .post('/insurance/user/byName')
      .expect(200)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .send({
        name: 'Corina'
      })
      .end(function (err, res) {
        assert.error(err, 'should not throw error');
        assert.deepEqual(res.body, expected, 'should respond with the information of user');
        assert.end();
      });
  });
});
