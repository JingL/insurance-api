const request = require('supertest');
const test = require('tape');

const { app, start } = require('../../lib/server');

const { SECRET } = require('../../lib/config/config');
const { InvalidSecret, InvalidContentType, MissingFieldId, MissingFieldName, UserNotFound,
  UserNotFoundOrInvalidUser, PolicyNotFound } = require('../../lib/errorCodes/errors');

test('/insurance/policy/byId', function (t) {

  t.test('with wrong secret', function (assert) {
    const expected = { error: InvalidSecret.message, success: false };

    request(app)
      .post('/insurance/policy/byId')
      .expect(InvalidSecret.code)
      .set('x-insurance-identity', 'test')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidSecret');
        assert.end();
      });
  });

  t.test('with wrong content-type', function (assert) {
    const expected = { error: InvalidContentType.message, success: false };

    request(app)
      .post('/insurance/policy/byId')
      .expect(InvalidContentType.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidContentType');
        assert.end();
      });
  });

  t.test('without body', function (assert) {
    const expected = { error: MissingFieldId.message, success: false };

    request(app)
      .post('/insurance/policy/byId')
      .expect(MissingFieldId.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with MissingFieldId');
        assert.end();
      });
  });

  t.test('with wrong policyId', function (assert) {
    const expected = { error: PolicyNotFound.message, success: false };

    request(app)
      .post('/insurance/policy/byId')
      .expect(PolicyNotFound.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .send({
        id: 'wrongId'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with PolicyNotFound');
        assert.end();
      });
  });

  t.test('with correct param and valid user', function (assert) {
    const expected = { result: { user: { id: 'a0ece5db-cd14-4f21-812f-966633e7be86', name: 'Britney', email: 'britneyblankenship@quotezart.com', role: 'admin' }}};

    request(app)
      .post('/insurance/policy/byId')
      .expect(200)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .send({
        id: '15b4430d-96f8-468e-98c0-3caaf8b0b3b6'
      })
      .end(function (err, res) {
        assert.error(err, 'should not throw error');
        assert.deepEqual(res.body, expected, 'should respond with the information of user');
        assert.end();
      });
  });
});

test('/insurance/policy/byUserName', function (t) {

  t.test('with wrong secret', function (assert) {
    const expected = { error: InvalidSecret.message, success: false };

    request(app)
      .post('/insurance/policy/byUserName')
      .expect(InvalidSecret.code)
      .set('x-insurance-identity', 'test')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidSecret');
        assert.end();
      });
  });

  t.test('with wrong content-type', function (assert) {
    const expected = { error: InvalidContentType.message, success: false };

    request(app)
      .post('/insurance/policy/byUserName')
      .expect(InvalidContentType.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with InvalidContentType');
        assert.end();
      });
  });

  t.test('without body', function (assert) {
    const expected = { error: MissingFieldName.message, success: false };

    request(app)
      .post('/insurance/policy/byUserName')
      .expect(MissingFieldName.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with MissingFieldName');
        assert.end();
      });
  });

  t.test('with wrong user name', function (assert) {
    const expected = { error: UserNotFoundOrInvalidUser.message, success: false };

    request(app)
      .post('/insurance/policy/byUserName')
      .expect(UserNotFoundOrInvalidUser.code)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .send({
        name: 'wrongName'
      })
      .end(function (err, res) {
        assert.error(err, 'Unauthorized');
        assert.deepEqual(res.body, expected, 'should respond with UserNotFoundOrInvalidUser');
        assert.end();
      });
  });

  t.test('with user name who does not have policy', function (assert) {
    const expected = { result: { userPolicies: [[ ]] } };

    request(app)
      .post('/insurance/policy/byUserName')
      .expect(200)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .send({
        name: 'Corina'
      })
      .end(function (err, res) {
        assert.error(err, 'should not throw error');
        assert.deepEqual(res.body, expected, 'should respond with an empty userPolicies');
        assert.end();
      });
  });

  t.test('with user who has policies', function (assert) {
    const expected = {
      id: '7b624ed3-00d5-4c1b-9ab8-c265067ef58b',
      amountInsured: 399.89,
      email: 'inesblankenship@quotezart.com',
      inceptionDate: '2015-07-06T06:55:49Z',
      installmentPayment: true,
      clientId: 'a0ece5db-cd14-4f21-812f-966633e7be86'
    };

    request(app)
      .post('/insurance/policy/byUserName')
      .expect(200)
      .set('x-insurance-identity', SECRET)
      .set('Content-Type', 'application/json')
      .send({
        name: 'Britney'
      })
      .end(function (err, res) {
        assert.error(err, 'should not throw error');
        assert.deepEqual(res.body.result.userPolicies[0][0], expected, 'should respond with the list of user policies');
        assert.end();
      });
  });
});
